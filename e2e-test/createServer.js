const Fastify = require('fastify');
const scheduler = require('node-schedule');
const axios = require('axios');
const bootstrap = require('../src/utils/bootstrap');
const knexConfig = require('../knexfile').testing;
const knex = require('knex')(knexConfig);


function createServer() {

  return new Promise((resolve, reject) => {
    const fastify = Fastify();

    const port = 4001;
    const host = '0.0.0.0';

    bootstrap({ fastify, knex, axios, scheduler });

    fastify.listen(port, host, (err) => {
      if (err) {
        fastify.log.error(err);
        reject(err);
        process.exit(1);
      } else {
        resolve({
          fastify,
          knex,
        });
      }
    });

  });
}

// createServer();
module.exports =  createServer;