const newman = require('newman');
const path = require('path');
const createServer = require('./createServer');
const collection = require('./postman_collection.json');


const fullPathOfTestCSV = path.resolve(__dirname, "./test.csv");
collection.item[0].request.body.formdata[0].src = fullPathOfTestCSV;
const newmanConfig = {
  collection, reporters: ['cli', 'html']
};

(async function () {
  const { knex } = await createServer();
  console.log('Clear testing DB before E2E tests');
  await knex('sales_records').delete();
  newman.run(newmanConfig, async (err, summary) => {
    if (err | summary.error) {
      console.error('Cannot pass all E2E tests');
    } else {
      console.log('Passed all E2E tests');
    }

    console.log('Start to tear down testing DB');

    try {
      await knex('sales_records').delete()
      console.log('Finished tearing down testing DB');
    } catch (error) {
      console.error(error);
      process.exit(1);

    }
    process.exit(0);
  });
})();
