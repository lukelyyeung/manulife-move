# Sales Record CSV upload


## 🚀 Getting Started

### 📲 Development

#### Env specific files

- Prepare `.env` file according to `.env.example`

`DB_NAME`, `DB_PASSWORD`, `DB_USER` can be any string

`DB_HOST` is tricky one, leave it blank unless you would like to build a backend image using `docker build`

`DATA_FEED_ENDPOINT` is the url to be called every hour by the DataFeedService

#### Scripts

- Please make sure you have docker / local postgresql installed

- To develop, please follow the below steps

1️⃣ `yarn install`

2️⃣ `docker-compose -d db`

3️⃣ `yarn knex migrate:latest`

4️⃣ `yarn dev`

> ℹ️ in case of using local postgresql, please skip step 2 and amend the .env variable accordingly

<hr/>

### Test

#### Unit test

`yarn test:unit`

#### E2E test

`yarn test:e2e`

> ℹ️ docker and docker-compose required

### ⚒ Build

Run `docker-compose up -d`

> ℹ️ remove `DB_HOST` in .env or use `db` as value

OR

Run `docker build`

This will build only the nodeJS application into image
> ℹ️ remember to update the .env to connect to an external database

### Thoughts on the task

#### Handle large file

The processing of processing huge .csv file and write into database has been quite a challenge.

The first thought was using stream to chunk the csv data and handle them bit by bit. However, when the file contains more millions of rows, the insertion time become a nightmare.

In my implementation, to improve the speed, I have pushed batchInsert promise into array and only await them at the end of the stream, this has improved the loading time from 30 mins to 15 ~ 20 mins for 1gb csv file, though with the trading off of handling promise rejection asynchronously.

#### Validation

The validation of csv data is done row by row, if there is invalid data, the row will be filtered instead of reject the whole file.

<hr/>
