
exports.up = function (knex) {
  return knex.schema.createTable('sales_records', (table) => {
    table.increments('id').primary();
    table.string('username');
    table.integer('age');
    table.integer('height');
    table.enum('gender', ['m', 'f']);
    table.float('sale_amount');
    table.datetime('last_purchase_date');
    table.timestamps(true, true);
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('sales_records');
};
