const { MulterError } = require("fastify-multer/lib");

function errorHandler(error, request, reply) {
  request.log.error(error);

  const message = error.message;
  if (error instanceof MulterError) {
    return reply.status(400).send({
      message
    });
  }

  if (error.isApp) {
    return reply.status(error.code || 500).send({
      message
    });
  }

  reply.status(500).send(error);
}

module.exports = errorHandler;
