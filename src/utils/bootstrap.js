const multer = require('fastify-multer');
const fastifyCors = require('fastify-cors');
const SalesRouter = require('../routes/SalesRouter');
const SalesController = require('../controllers/SalesController');
const SalesService = require('../services/SalesService');
const SalesRepository = require('../repositories/SalesRepository');
const DataFeedService = require('../services/DataFeedService');
const loggerHook = require('./loggerHook');
const errorHandler = require('./errorHandler');

module.exports = ({ fastify, knex, dataFeedEndpoint, axios, scheduler }) => {
  fastify.register(fastifyCors);
  fastify.addHook('onRequest', loggerHook);
  fastify.register(multer.contentParser);
  fastify.setErrorHandler(errorHandler);

  const salesRepository = new SalesRepository(knex);
  const salesService = new SalesService(salesRepository);
  const salesController = new SalesController(salesService);
  const salesRouter = new SalesRouter(salesController);
  const dataFeedService = new DataFeedService({
    endpoint: dataFeedEndpoint,
    axios,
    scheduler,
    salesService,
  });

  dataFeedService.run({ minute: 0 });
  fastify.register(salesRouter.get(), { prefix: salesRouter.prefix });
}