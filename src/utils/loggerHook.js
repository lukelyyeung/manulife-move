function loggerHook(req, reply, done) {
  req.log.info({
    url: req.url,
    method: req.method,
    body: req.body,
  });

  done();
}

module.exports = loggerHook;
