function createError(message, { code }) {
  const error = new Error(message);
  error.code = code;
  error.isApp = true;

  return error;
}

module.exports = createError;
