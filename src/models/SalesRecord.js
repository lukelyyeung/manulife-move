class SalesRecord {
  constructor(payload) {
    this.username = payload.username;
    this.age = payload.age;
    this.height = payload.height;
    this.gender = payload.gender;
    this.saleAmount = payload.saleAmount;
    this.lastPurchaseDate = payload.lastPurchaseDate;
  }

  static fromDB(payload) {
    return new SalesRecord({
      username: payload.username,
      age: payload.age,
      height: payload.height,
      gender: payload.gender,
      saleAmount: payload.sale_amount,
      lastPurchaseDate: payload.last_purchase_date,
    });
  }

  toDB() {
    return {
      username: this.username,
      age: this.age,
      height: this.height,
      gender: this.gender,
      sale_amount: this.saleAmount,
      last_purchase_date: this.lastPurchaseDate,
    }
  }
}

module.exports = SalesRecord;
