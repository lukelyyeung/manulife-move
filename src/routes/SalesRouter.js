const upload = require('fastify-multer')({ dest: 'uploads/' });

class SalesRouter {
  constructor(salesController) {
    this.prefix = '/sales';
    this.salesController = salesController;
  }

  get() {
    return (fastify, opts, done) => {
      fastify.get('/report', this.salesController.getReports);
      fastify.post('/record', { preHandler: upload.single('file') }, this.salesController.saveRecord);
      done();
    }
  }
}

module.exports = SalesRouter;
