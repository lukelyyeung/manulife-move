const Fastify = require('fastify');
const bootstrap = require('./utils/bootstrap');
const scheduler = require('node-schedule');
const axios = require('axios');
const nodeEnv = process.env.NODE_ENV ?? 'development';
const knexConfig = require('../knexfile')[nodeEnv];
const knex = require('knex')(knexConfig);

const dataFeedEndpoint = process.env.DATA_FEED_ENDPOINT;
const fastify = Fastify({
  logger: nodeEnv !== 'production',
});

const port = process.env.PORT || 4000;
const host = '0.0.0.0';

bootstrap({ fastify, knex, axios, scheduler, dataFeedEndpoint });

fastify.listen(port, host, (err, address) => {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
});