class SalesService {
  constructor(salesRepository) {
    this.salesRepository = salesRepository;
  }

  async saveRecord(data) {
    return this.salesRepository.saveRecord(data);
  }

  async getReports(query) {
    return this.salesRepository.getReports(query);
  }
}

module.exports = SalesService;
