class DataFeedService {
  constructor({ endpoint, axios, scheduler }) {
    this.endpoint = endpoint;
    this.scheduler = scheduler;
    this.axios = axios;
  }

  run(recurrenceRule) {
    this.scheduledJob = this.scheduler.scheduleJob(recurrenceRule, async () => {
      try {
        const res = await this.axios.get(this.endpoint);
        console.log('Datafeed triggered, response:', res.data);
      } catch (error) {
        console.log(error);
      }
    });
  }

  stop() {
    if (this.scheduledJob) {
      scheduledJob.cancel();
    }
  }
}

module.exports = DataFeedService;
