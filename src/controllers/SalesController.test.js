const SalesController = require('./SalesController');
const SalesRecord = require('../models/SalesRecord');
const fs = require('fs');
const { EOL } = require('os');
const { parse } = require('fast-csv');


jest.mock('fs');

describe('SalesController', () => {

  const mockReports = {
    totalCount: 1,
    totalPage: 1,
    result: [
      new SalesRecord({
        username: 'username',
        age: 27,
        height: 180,
        gender: 'f',
        saleAmount: 90000,
        lastPurchaseDate: '2021-01-16',
      })
    ]
  };

  describe('#getReports', () => {
    let mockSalesService;

    beforeEach(() => {
      mockSalesService = {
        getReports: jest.fn().mockResolvedValue(mockReports),
        saveRecord: jest.fn().mockResolvedValue(1000),
      }
    });

    test('should delegate result of SalesService#getReports to reply.send', async () => {
      const mockRequest = { query: {} };
      const mockDone = jest.fn();
      const mockReply = {
        send: jest.fn(),
      };
      const saleController = new SalesController(mockSalesService);

      await saleController.getReports(mockRequest, mockReply, mockDone);

      expect(mockReply.send.mock.calls[0][0]).toMatchObject(mockReports)
    });

    test('should return transformed db query result', async () => {
      const mockRequest = {
        query: {
          from: '2021-01-14',
          to: '2021-01-16',
          date: '2021-01-16',
          pageSize: 50,
          page: 1,
        }
      };
      const mockDone = jest.fn();
      const mockReply = {
        send: jest.fn(),
      };
      const saleController = new SalesController(mockSalesService);

      await saleController.getReports(mockRequest, mockReply, mockDone);

      expect(mockSalesService.getReports.mock.calls[0][0]).toMatchObject(mockRequest.query);
    });

    test('should throw error when query parameter is invalid', async () => {
      const mockRequest = {
        query: {
          pageSize: 'not an integer',
        }
      };

      const mockDone = jest.fn();
      const mockReply = {
        send: jest.fn(),
      };
      const saleController = new SalesController(mockSalesService);

      try {
        await saleController.getReports(mockRequest, mockReply, mockDone);
        throw new Error('Should not pass');
      } catch (error) {
        expect(error.message).toBe('Invalid pageSize');
        expect(error.code).toBe(400);
      }
    });
  });

  describe('saveRecord', () => {
    let mockRequest;
    let mockReply;

    beforeEach(() => {
      mockSalesService = {
        saveRecord: jest.fn().mockResolvedValue(1000),
      };

      const csvString = [
        'USER_NAME,AGE,HEIGHT,GENDER,SALE_AMOUNT,LAST_PURCHASE_DATE',
        'John Doe,42,142,m,2759,2021-02-08T04:40:34.390Z',
      ].join(EOL);

      const stream = parse({ headers: true });
      stream.write(csvString);
      stream.end();

      fs.unlinkSync.mockClear();
      fs.createReadStream.mockClear();
      fs.createReadStream.mockReturnValue({
        pipe: jest.fn().mockReturnValue(
          stream
        )
      });

      afterAll(() => {
        jest.clearAllMocks();
      })

      mockRequest = {
        file: {
          path: 'mockPath',
          mimetype: 'text/csv',
        }
      }

      mockReply = {
        status: jest.fn().mockReturnThis(),
        send: jest.fn(),
      }
    });


    test('should delegate readStream created from request.file.path to SalesService#saveRecord', async () => {

      const salesController = new SalesController(mockSalesService);

      await salesController.saveRecord(
        mockRequest,
        mockReply,
        () => { },
      );


      const convertedCSV = await new Promise((resolve) => {
        const buffer = [];
        const stream = mockSalesService.saveRecord.mock.calls[0][0];
        stream.on('data', (chunk) => buffer.push(chunk));
        stream.on('end', () => {
          resolve(buffer);
        });
      });

      expect(convertedCSV).toMatchObject([
        {
          username: 'John Doe',
          age: 42,
          height: 142,
          gender: 'm',
          sale_amount: 2759,
          last_purchase_date: '2021-02-08T04:40:34.390Z'
        }
      ]);

      expect(fs.unlinkSync).toBeCalled();
      expect(mockReply.status.mock.calls[0][0]).toBe(201);
      expect(mockReply.send.mock.calls[0][0]).toMatchObject({
        numberOfRecordCreated: 1000,
      })
    });
    test('should sanitize the parsed csv', async () => {
      const csvString = [
        'USER_NAME,AGE,HEIGHT,GENDER,SALE_AMOUNT,LAST_PURCHASE_DATE',
        ',,,,,',
      ].join(EOL);

      const stream = parse({ headers: true });
      stream.write(csvString);
      stream.end();

      fs.createReadStream.mockReturnValue({
        pipe: jest.fn().mockReturnValue(
          stream
        )
      });

      const salesController = new SalesController(mockSalesService);

      await salesController.saveRecord(
        mockRequest,
        mockReply,
        () => { },
      );


      const convertedCSV = await new Promise((resolve) => {
        const buffer = [];
        const stream = mockSalesService.saveRecord.mock.calls[0][0];
        stream.on('data', (chunk) => buffer.push(chunk));
        stream.on('end', () => {
          resolve(buffer);
        });
      });

      expect(convertedCSV).toMatchObject([]);

      expect(fs.unlinkSync).toBeCalled();
    });
    test('should still call unlinkSync if error', async () => {

      const salesController = new SalesController(mockSalesService);
      fs.createReadStream.mockImplementation(() => {
        throw new Error();
      })

      try {
        await salesController.saveRecord(
          mockRequest,
          mockReply,
          () => { },
        );
        throw new Error('Should not pass');
      } catch (error) {
        expect(fs.unlinkSync).toBeCalled();
      }
    });
  })
});