const { Transform } = require('stream');
const { createReadStream, unlinkSync } = require('fs');
const { DateTime } = require('luxon');
const { parse } = require('fast-csv');
const createError = require('../utils/createError');
const SalesRecord = require('../models/SalesRecord');

class SalesController {
  constructor(salesService) {
    this.salesService = salesService;
    this.saveRecord = this.saveRecord.bind(this);
    this.getReports = this.getReports.bind(this);
  }

  async saveRecord(req, reply, done) {
    if (!req.file) {
      console.log(req);
      throw createError('Missing file', { code: 400 });
    }

    if (req.file.mimetype !== "text/csv") {
      unlinkSync(req.file.path);
      throw createError('Invalid file type', { code: 400 });
    }

    const csvPath = req.file.path;
    try {
      const readStream = createReadStream(csvPath)
        .pipe(
          parse({ headers: true }))
        .pipe(
          new Transform({
            objectMode: true,
            transform: (row, encoding, done) => {
              const sanitizedPayload = this._sanitizeSalesRecord(row);
              const transformedRow = new SalesRecord(sanitizedPayload).toDB();
              done(null, Object.keys(sanitizedPayload).length > 0 ? transformedRow : null);
            }
          }))

      const numberOfRecordCreated = await this.salesService.saveRecord(readStream);
      unlinkSync(req.file.path);
      reply.status(201).send({ numberOfRecordCreated });
    } catch (error) {
      unlinkSync(req.file.path);
      throw error;
    }

  }

  _sanitizeSalesRecord({
    USER_NAME,
    AGE,
    HEIGHT,
    GENDER,
    SALE_AMOUNT,
    LAST_PURCHASE_DATE,
  }) {
    const result = {};

    if (USER_NAME && typeof USER_NAME === 'string') {
      result.username = USER_NAME;
    }

    if (AGE && typeof AGE === 'string' && Number.isInteger(+AGE)) {
      result.age = +AGE;
    }

    if (GENDER && typeof GENDER === 'string' && ['f', 'm'].includes(GENDER.toLowerCase())) {
      result.gender = GENDER.toLowerCase();
    }

    if (HEIGHT && typeof HEIGHT === 'string' && Number.isInteger(+HEIGHT)) {
      result.height = +HEIGHT;
    }

    if (SALE_AMOUNT && typeof SALE_AMOUNT === 'string' && !Number.isNaN(+SALE_AMOUNT)) {
      result.saleAmount = +SALE_AMOUNT;
    }

    if (LAST_PURCHASE_DATE && DateTime.fromISO(LAST_PURCHASE_DATE).isValid) {
      result.lastPurchaseDate = LAST_PURCHASE_DATE;
    }

    return result;
  }

  async getReports(req, reply, done) {
    this._validateGetReportQueryParameter(req.query);
    const { from, to, date, pageSize, page } = req.query;
    const result = await this.salesService.getReports({ from, to, date, pageSize, page });
    reply.send(result);
  }

  _validateGetReportQueryParameter({ from, to, date, pageSize, page }) {
    if (date && !DateTime.fromISO(date).isValid) {
      throw createError('date must be ISO string', { code: 400 });
    }

    if (from && !DateTime.fromISO(from).isValid) {
      throw createError('from must be ISO string', { code: 400 });
    }

    if (to && !DateTime.fromISO(to).isValid) {
      throw createError('to must be ISO string', { code: 400 });
    }

    if (pageSize && (+pageSize < 0 || !Number.isInteger(+pageSize))) {
      throw createError('Invalid pageSize', { code: 400 });
    }

    if (page && (+page < 0 || !Number.isInteger(+page))) {
      throw createError('Invalid page', { code: 400 });
    }
  }

}

module.exports = SalesController;
