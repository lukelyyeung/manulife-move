const { Writable, pipeline, } = require('stream');
const { promisify } = require('util');
const SalesRecord = require("../models/SalesRecord");

const pump = promisify(pipeline);

class SalesRepository {
  constructor(knex) {
    this.tableName = 'sales_records';
    this.knex = knex;
  }

  async saveRecord(stream) {
    let numberOfRow = 0;
    let rollback;

    try {
      await this.knex.transaction((trx) => {
        rollback = trx.rollback;
        let bufferedRows = [];
        const savePromises = [];

        const pushBatchInsertPromise = () => {
          const savePromise = trx.batchInsert(this.tableName, [...bufferedRows])
            .returning('id')
            .then((ids) => numberOfRow += ids.length);
          bufferedRows = [];
          savePromises.push(savePromise);
        }

        return pump(stream, new Writable({
          objectMode: true,
          write: async (row, encoding, done) => {
            if (!Object.keys(row).length) {
              done();
              return;
            }

            bufferedRows.push(row);
            if (bufferedRows.length >= 1000) {
              pushBatchInsertPromise();
            }
            done();
          }
        }))
          .then(() => {
            if (bufferedRows.length > 0) {
              pushBatchInsertPromise();
            }

            return Promise.all(savePromises)
              .then(trx.commit)
              .catch(err => {
                trx.rollback();
                throw err;
              });
          });
      });
    } catch (error) {
      rollback();
      throw error;
    }

    return numberOfRow;
  }

  async getReports({ page = 1, pageSize = 50, date, from, to } = {}) {
    const [{ count }] = await this.knex(this.tableName)
      .modify((queryBuilder) =>
        this._modifyQueryBuilderByDate(queryBuilder, 'last_purchase_date', { from, to, date }))
      .count();

    const totalPage = Math.ceil(+count / pageSize);
    const offset = pageSize * (page - 1);

    const items = await this.knex(this.tableName)
      .modify((queryBuilder) =>
        this._modifyQueryBuilderByDate(queryBuilder, 'last_purchase_date', { from, to, date }))
      .offset(offset)
      .limit(pageSize)
      .orderBy('last_purchase_date', 'desc')

    return {
      totalPage,
      totalCount: +count,
      result: items.map(SalesRecord.fromDB),
    }
  }

  _modifyQueryBuilderByDate(queryBuilder, column, { from, to, date }) {
    if (date) {
      return queryBuilder.whereBetween(column, [date, date]);
    }

    if (from && to) {
      return queryBuilder.whereBetween(column, [from, to]);
    }
  };
}

module.exports = SalesRepository;
