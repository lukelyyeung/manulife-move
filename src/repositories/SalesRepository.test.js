const SalesRepository = require('./SalesRepository');
const { Readable } = require('stream');
const SalesRecord = require('../models/SalesRecord');

describe('SalesRepository', () => {
  const mockDBQueryResult = [
    {
      username: 'username',
      age: 'age',
      height: 'height',
      gender: 'gender',
      sale_amount: 'sale_amount',
      last_purchase_date: 'last_purchase_date',
    }
  ];

  describe('#getReports', () => {
    let mockKnexInstance;

    beforeEach(() => {

      mockKnexInstance = {
        orderBy: jest.fn().mockResolvedValueOnce(mockDBQueryResult),
        whereBetween: jest.fn(),
        modify: jest.fn((func) => {
          func(mockKnexInstance);
          return mockKnexInstance;
        }),
        count: jest.fn().mockResolvedValueOnce([{ count: 100 }]),
        offset: jest.fn().mockReturnThis(),
        limit: jest.fn().mockReturnThis(),
      }

    })

    test('should return transformed db query result', async () => {

      const salesRepository = new SalesRepository(() => mockKnexInstance);

      const response = await salesRepository.getReports();

      expect(response).toMatchObject({
        result: mockDBQueryResult.map(SalesRecord.fromDB),
        totalCount: 100,
        totalPage: 2,
      })
    });


    test('should support date option', async () => {

      const salesRepository = new SalesRepository(() => mockKnexInstance);
      const response = await salesRepository.getReports({ date: '2021-01-16' });

      expect(mockKnexInstance.whereBetween.mock.calls[0][1]).toMatchObject(['2021-01-16', '2021-01-16']);

      expect(response).toMatchObject({
        result: mockDBQueryResult.map(SalesRecord.fromDB),
        totalCount: 100,
        totalPage: 2,
      })
    });

    test('should support to and from options', async () => {

      const salesRepository = new SalesRepository(() => mockKnexInstance);
      const response = await salesRepository.getReports({ from: '2021-01-16', to: '2021-01-20' });

      expect(mockKnexInstance.whereBetween.mock.calls[0][1]).toMatchObject(['2021-01-16', '2021-01-20']);

      expect(response).toMatchObject({
        result: mockDBQueryResult.map(SalesRecord.fromDB),
        totalCount: 100,
        totalPage: 2,
      })
    });

    test('should support pageSize and page options', async () => {

      const salesRepository = new SalesRepository(() => mockKnexInstance);

      const response = await salesRepository.getReports({ pageSize: 20, page: 2 });

      expect(mockKnexInstance.offset.mock.calls[0][0]).toBe(20);
      expect(mockKnexInstance.limit.mock.calls[0][0]).toBe(20);

      expect(response).toMatchObject({
        result: mockDBQueryResult.map(SalesRecord.fromDB),
        totalCount: 100,
        totalPage: 5,
      })
    });
  });

  describe('#saveRecords', () => {
    const numberOfRecords = 10000;
    let readable;
    let mockTrx;
    let insertedRecords;

    beforeEach(() => {
      insertedRecords = [];
      readable = new Readable.from(
        Array.from(new Array(numberOfRecords).keys()).map((username) => new SalesRecord({
          username,
          age: 27,
          height: 180,
          gender: 'm',
          saleAmount: 9000,
          lastPurchaseDate: '2020-11-05T13:15:30.000Z',
        })), { objectMode: true }
      );

      mockTrx = {
        commit: jest.fn(),
        rollback: jest.fn(),
        batchInsert: jest.fn((_, records) => {
          insertedRecords = insertedRecords.concat(records);
          return ({ returning: jest.fn().mockResolvedValueOnce(records) })
        }),
      }

    })


    test('should save data from a object stream', async () => {

      const salesRepository = new SalesRepository({
        transaction: jest.fn().mockImplementation(async (func) => func(mockTrx))
      });

      const response = await salesRepository.saveRecord(readable);
      expect(response).toBe(numberOfRecords);
      expect(new Set(insertedRecords.map(({ username }) => username)).size).toBe(numberOfRecords)
      expect(mockTrx.batchInsert.mock.calls.length).toBe(Math.ceil(numberOfRecords / 1000));
      expect(mockTrx.commit).toBeCalled();
    });

    test('should rollback when error during transaction', async () => {

      mockTrx.batchInsert = jest.fn((_, records) => {
        return ({
          returning: jest.fn().mockImplementation(() => {
            return Promise.reject(new Error());
          })
        })
      });

      const salesRepository = new SalesRepository({
        transaction: jest.fn().mockImplementation(async (func) => func(mockTrx))
      });

      readable = new Readable.from(
        Array.from(new Array(numberOfRecords).keys()).map((username) => new SalesRecord({
          username,
          age: 27,
          height: 180,
          gender: 'm',
          saleAmount: 9000,
          lastPurchaseDate: '2020-11-05T13:15:30.000Z',
        })).concat([{}]), { objectMode: true }
      );
      try {
        await salesRepository.saveRecord(readable);
        throw new Error('Should not pass');
      } catch (error) {
        expect(mockTrx.rollback).toBeCalled();
      }
    });
  })
});