FROM node:14

WORKDIR /usr/app

COPY package.json /usr/app
COPY yarn.lock /usr/app

RUN yarn install --quiet

COPY . .

EXPOSE 4000

CMD [ "yarn", "start" ]
