// Update with your config settings.
require('dotenv').config();

const {
  DB_NAME,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  HOME,
} = process.env;

const connectionConfig = {
  // Running knex cli in docker require container name to access
  host: DB_HOST ?? (HOME === '/root' ? 'db' : '127.0.0.1'),
  database: DB_NAME,
  user: DB_USER,
  password: DB_PASSWORD
}

module.exports = {
  development: {
    client: 'pg',
    connection: connectionConfig,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  testing: {
    client: 'pg',
    connection: {
      database: 'test',
      password: 'test_password',
      user: 'test_user',
      port: 6000,
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  staging: {
    client: 'pg',
    connection: connectionConfig,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'pg',
    connection: connectionConfig,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
